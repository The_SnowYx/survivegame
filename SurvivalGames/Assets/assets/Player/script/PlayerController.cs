﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //// Component ////
    Rigidbody rigbodyPlayer;
	public Camera m_cam;

    //// Valeurs des Axes de deplacement / Jump ////
    private float ValAxisHorizontal;
    private float ValAxisVertical;
    private float ValAxisJump;

    //// Valeurs de Vitesse du Player ///
    public float WalkSpeed;

    //// Valeur de Jump disponible ////
    private int JumpCount;

    //// Valeur Hauteur de saut ////
    public float JumpStrengh;
    public float Gravity;

	/// Valeur Sensibilité //
	public float Cam_sensi;
	private float yaw;
	private float pitch;

    void Start () {
        JumpCount = 1;
        rigbodyPlayer = GetComponent<Rigidbody>();
        Physics.gravity = new Vector3(0, Gravity, 0);
        Cursor.lockState = CursorLockMode.Locked;
	}
	
	void Update () {

        //// Recuperation des Axe de deplacement / Jump ////
        ValAxisHorizontal = Input.GetAxis("Horizontal");
        ValAxisVertical = Input.GetAxis("Vertical");
        ValAxisJump = Input.GetAxis("Jump");

        //// Deplacement sur les axes Z X ///
        double Tempspeed;
        /// Z ///
        Tempspeed = WalkSpeed;
        if (ValAxisVertical < 0) { Tempspeed = Tempspeed / 1.5; }
        transform.Translate(0, 0, ValAxisVertical*(float)Tempspeed);
        /// X ///
        Tempspeed = WalkSpeed / 1.5;
        transform.Translate(ValAxisHorizontal* (float)Tempspeed, 0, 0);


        //// Jump ////
        if(ValAxisJump>0 && JumpCount > 0)
        {
            Vector3 v = rigbodyPlayer.velocity;
            v.y = JumpStrengh;
            rigbodyPlayer.velocity = new Vector3(0, JumpStrengh, 0);
        }

		/// Mouvement Camera ///
		pitch -= Cam_sensi * (Input.GetAxis("Mouse Y")/10);
		yaw += Cam_sensi * (Input.GetAxis("Mouse X")/10);
		if (pitch > 80)pitch = 80;
		if (pitch < -80)pitch = -80;
		m_cam.transform.eulerAngles = new Vector3(pitch,yaw,0.0f);
		transform.eulerAngles = new Vector3 (0.0f, yaw, 0.0f);
    }

    public void SetJumpCount(int count) { JumpCount = count; }




}
