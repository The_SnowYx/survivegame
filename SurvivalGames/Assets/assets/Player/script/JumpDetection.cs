﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpDetection : MonoBehaviour {

    GameObject ScriptParent; 

	void Start () {
        ScriptParent = transform.parent.gameObject;
	}

    public void OnTriggerEnter (Collider col)
    {
        ScriptParent.GetComponent<PlayerController>().SetJumpCount(1);
    }
    public void OnTriggerExit (Collider col)
    {
        ScriptParent.GetComponent<PlayerController>().SetJumpCount(0);
    }

}
